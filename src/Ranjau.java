import java.util.Scanner;
import java.util.Random;

public class Ranjau {
    public static void main(String[] args) {
        //Variable yang dibutuhkan
        int baris = 0;
        int kolom = 0;
        int mine = 0;

        System.out.println("##############################################");
        System.out.println("Selamat datang di Permainan Ranjau Tersembunyi");
        System.out.println("Dibuat oleh: Rizky Liyanoviar");
        System.out.println("##############################################");
        Scanner input = new Scanner(System.in);
        System.out.print("Masukan ukuran field: ");
        int field = input.nextInt();
        System.out.print("Pilih level permainan (1: SULIT; 2: MUDAH): ");
        String level = input.next();

        if (level.equals("1")) { //Jika Level yang dipilih 1 (Sulit)
            mine = (field * field + 1) / 2; //Jumlah ranjau = (field*field+1)/2
        }
        else if (level.equals("2")) { //Jika Level yang dipilih 2 (Mudah)
            mine = (field * field + 3) / 4; //Jumlah ranjau = (field*field+3)/4
        }
        else { //Jika level yang dipilih tidak sesuai dengan yang ditentukan
            System.out.println("Level yang dipilih tidak ada");
        }

        baris = field; //Menentukan jumlah baris sama dengan jumlah field yang telah diinputkan
        kolom = field; //Menentukan jumlah kolom sama dengan jumlah field yang telah diinputkan
        int[][] board = new int[baris][kolom]; // Membuat objek array untuk board untuk nilai papan minesweeper
        int counter = mine; //Inisialisasi nilai counter sesuai dengan nilai mine
        while (counter > 0){ // Perulangan untuk membuat papan minesweeper sebanyak counter
            Random rand = new Random(); //Objeck random untuk menampung ranjau
            baris = rand.nextInt(field);  // Mengacak bom di baris tertentu
            kolom = rand.nextInt(field);  // Mengacak bom di kolom tertentu
            if (board[baris][kolom] == 0){ // Pengecekkan apakah sebuah lokasi papan bernilai nol/tidak ada bom
                board[baris][kolom] = 1;
                counter--; // Mengurangi nilai counter hingga menghentikan looping
            }
        }

        boolean finish = false; // Mendeklarasi variable finish untuk berhenti bertipe boolean dengan nilai awal false
        while(!finish){ // Perulangan selama nilai finish true
            System.out.println("Ini adalah kotak permainan Anda:");
            for (baris = 0; baris < board.length; baris++) {
                for (kolom = 0; kolom < board[baris].length; kolom++) {
                    if (board[baris][kolom]==2) { // Melakukan pengecekkan untuk merubah nilai posisi yang di pilih dengan karakter "O"
                        System.out.print("O  ");
                    }
                    else {
                        System.out.print((baris + 1) + "" + (kolom + 1) + " ");
                    }
                }
                System.out.println();
            }
            System.out.println("Jumlah Ranjau: " + mine);
            System.out.println();
            System.out.print("Silahkan pilih posisi sel: ");
            String position = input.next();
            System.out.println();
            baris = Integer.parseInt(position.charAt(0)+"")-1; // Memasukkan nilai posisi ke variable baris
            kolom = Integer.parseInt(position.charAt(1)+"")-1; // Memasukkan nilai posisi ke variable kolom
            if (board[baris][kolom]==1) { // Mengecek nilai sebuah posisi apakah 1/0
                for (int i = 0; i < field; i++) { // Menampilkan bom lain
                    for (int j = 0; j < field; j++) {
                        if (board[i][j] == 1) {
                            System.out.print("X  ");
                        } else if (board[i][j] == 2) {
                            System.out.print("O  ");
                        } else {
                            System.out.print((i + 1) + "" + (j + 1) + " ");
                        }
                    }
                    System.out.println();
                }
                finish = true; // Nilai finish berubah true untuk menghentikan perulangan
                System.out.println("\nMaaf, Anda Kalah!");
                break;
            }
            else if(board[baris][kolom]==0) { // Jika yang di pilih masih 0
                board[baris][kolom] = 2;
            }
            counter++; // Menambah otomatis nilai counter
            if (counter==field) { // Mengecek apakah nilai counter sesuai dengan mine untuk menentukan kemenangan
                finish = true; // Merubah nilai finish untuk menghentikan perulangan
                System.out.println("\nSelamat Anda menang!");
            }
        }
        input.close(); // Menghentikan permintaan input
    }
}
